from st3m.reactor import Responder
from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
import leds
import captouch
import math
import network
import socket

class Example(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.pos = 0;
        self.r = 255;
        self.g = 255;
        self.b = 255;
        self.wifi = network.WLAN(network.STA_IF)
        self.wifi.active(True)
        self.wifi.connect('Camp2023-open')
        self.socket = None
        self.t = 0
        self.deadline = 0
        self.disabled = False
        self.last = [0, 0, 0]

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.move_to(-90,-10)
        ctx.rgb(255, 255, 255).text("r " + str(self.r))
        ctx.move_to(-90,20)
        ctx.rgb(255, 255, 255).text("g " + str(self.g))
        ctx.move_to(-90,50)
        ctx.rgb(255, 255, 255).text("b " + str(self.b))

        if self.wifi.isconnected():
            ctx.move_to(-50,-60).rgb(0, 120, 0).text('jacked in')
        elif self.disabled:
            ctx.move_to(-50,-60).rgb(0, 0, 255).text('restrained')
        else:
            ctx.move_to(-50,-60).rgb(255, 0, 0).text('thonking')

        ctx.move_to(-50,90)
        ctx.rgb(100, 100, 100).text("v1 ~eta")


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        state = captouch.read()
        pos = int(state.petals[0].position[0] / 10000)
        self.r = max(min(255, self.r + pos), 0)

        pos = int(state.petals[2].position[0] / 10000)
        self.g = max(min(255, self.g + pos), 0)

        pos = int(state.petals[4].position[0] / 10000)
        self.b = max(min(255, self.b + pos), 0)
        leds.set_all_rgb(self.r / 255, self.g / 255, self.b / 255)
        leds.update()

        self.t += delta_ms

        if self.socket is None and self.wifi.isconnected():
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        if self.socket and not self.disabled and self.t > self.deadline and self.last != [self.r, self.g, self.b]:
            try:
                self.socket.sendto(bytearray([0xDE, 0xAD, 0xBE, 0xEF, self.r, self.g, self.b]), ("151.216.192.14", 4444))
                self.last = [self.r, self.g, self.b]
            except:
                pass

        while self.deadline < self.t:
            self.deadline += 150


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(Example(ApplicationContext()))
