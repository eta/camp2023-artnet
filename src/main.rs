use artnet_protocol::{ArtCommand, Output};
use rouille::{router, try_or_400, Response};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::net::UdpSocket;
use std::sync::{Arc, Mutex, RwLock};

pub static GOVUK_CSS: &[u8] = include_bytes!("../web/govuk.css");
pub static INDEX_HTML: &str = include_str!("../web/index.html");

#[derive(Debug)]
pub struct InvalidInputError(&'static str);

impl Display for InvalidInputError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl Error for InvalidInputError {}

fn err(i: &'static str) -> InvalidInputError {
    InvalidInputError(i)
}

fn parse_colour_hex(colour: &str) -> Result<(u8, u8, u8), InvalidInputError> {
    let r = colour.get(1..3).ok_or_else(|| err("no red channel"))?;
    let r = u8::from_str_radix(r, 16).map_err(|_| err("invalid red channel"))?;
    let g = colour.get(3..5).ok_or_else(|| err("no green channel"))?;
    let g = u8::from_str_radix(g, 16).map_err(|_| err("invalid red channel"))?;
    let b = colour.get(5..7).ok_or_else(|| err("no blue channel"))?;
    let b = u8::from_str_radix(b, 16).map_err(|_| err("invalid red channel"))?;
    Ok((r, g, b))
}

fn main() -> Result<(), anyhow::Error> {
    /*
    let socket = UdpSocket::bind(("10.231.12.27", 6454)).unwrap();

    let mut data = vec![0; 512];

    for ch in [1, 11, 21, 31, 41, 51, 61, 71, 81, 91] {
        let ch = ch - 1;
        data[ch] = 255;
        data[ch + 1] = 0;
        data[ch + 2] = 255;
        data[ch + 3] = 0;
    }

    let command = ArtCommand::Output(Output {
        data: data.into(),
        ..Output::default()
    });
    let bytes = command.write_to_buffer().unwrap();
    socket.send_to(&bytes, "10.231.12.12:6454").unwrap();
    return;

    let socket2 = UdpSocket::bind((Ipv4Addr::BROADCAST, 6454)).unwrap();
    let broadcast_addr = ("255.255.255.255", 6454)
        .to_socket_addrs()
        .unwrap()
        .next()
        .unwrap();
    socket.set_broadcast(true).unwrap();
    let buff = ArtCommand::Poll(Poll::default()).write_to_buffer().unwrap();
    socket.send_to(&buff, &broadcast_addr).unwrap();
    loop {
        let mut buffer = [0u8; 1024];
        let (length, addr) = socket2.recv_from(&mut buffer).unwrap();
        let command = ArtCommand::from_buffer(&buffer[..length]).unwrap();

        println!("{addr:?}: {:?}", command);
    }
    return;
     */
    eprintln!("[+] binding udp socket");
    let socket = UdpSocket::bind(("0.0.0.0", 6454))?;
    eprintln!("[+] starting on localhost:8080");
    let data = Arc::new(Mutex::new(vec![0; 512]));
    let colour = Arc::new(RwLock::new("#000000".to_string()));

    let update = move |data: Vec<u8>| {
        let command = ArtCommand::Output(Output {
            data: data.into(),
            port_address: 2.into(),
            ..Output::default()
        });
        let bytes = command.write_to_buffer().unwrap();
        socket.send_to(&bytes, "10.231.12.12:6454").unwrap();
    };

    rouille::start_server("0.0.0.0:80", move |request| {
        router!(request,
            (GET) ["/"] => {
                let mut thing = INDEX_HTML.replace("%COLOUR%", &colour.read().unwrap());
                if request.get_param("uncheck").is_some() {
                    thing = thing.replace(" checked>", ">");
                }
                Response::from_data("text/html", thing)
            },
            (GET) ["/illuminate2"] => {
                let allowed_chs = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90];
                let mut data = data.lock().unwrap();
                let mut chs = vec![];
                for ch in allowed_chs {
                    if let Some(colour) = request.get_param(&format!("light-{}", ch)) {
                        let (r, g, b) = try_or_400!(parse_colour_hex(&colour));
                        chs.push(ch);
                        data[ch] = 255;
                        data[ch + 1] = r;
                        data[ch + 2] = g;
                        data[ch + 3] = b;
                    }
                }
                eprintln!("{}: manual API call for {chs:?}", request.remote_addr());
                update(data.clone());
                Response::text("done.")
            },
            (GET) ["/illuminate"] => {
                let mut colour = colour.write().unwrap();
                let mut data = data.lock().unwrap();
                let param = try_or_400!(request.get_param("colour").ok_or_else(|| err("no colour param")));
                let (r, g, b) = try_or_400!(parse_colour_hex(&param));
                *colour = param.to_string();

                let mut chs = vec![];
                let allowed_chs = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90];
                for ch in allowed_chs {
                    if request.get_param(&format!("light-{}", ch)).is_some() {
                        chs.push(ch);
                        data[ch] = 255;
                        data[ch + 1] = r;
                        data[ch + 2] = g;
                        data[ch + 3] = b;
                    }
                }
                eprintln!("{}: updating lights {chs:?} to {param}", request.remote_addr());

                update(data.clone());
                Response::redirect_302("/")
            },
            (GET) ["/govuk.css"] => {
                Response::from_data("text/css", GOVUK_CSS)
            },
            _ => {
                Response::empty_404()
            }
        )
    });
}
