package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func main() {
	a, _ := net.ResolveUDPAddr("udp", "0.0.0.0:4444")
	l, err := net.ListenUDP("udp", a)
	if err != nil {
		log.Fatalf("uh %v", err)
	}

	goPls := make(chan []byte)
	go func() {
		for p := range goPls {
			param := make(url.Values)
			for i := 0; i < 10; i++ {
				param.Add(fmt.Sprintf("light-%d", i*10), fmt.Sprintf("%d", i*10))
			}
			param.Set("colour", fmt.Sprintf("#%01x", p))
			r, err := http.DefaultClient.Get(fmt.Sprintf("http://domelights.i.eta.st/illuminate?%s", param.Encode()))
			if err != nil {
				continue
			}
			io.ReadAll(r.Body)
		}
	}()

	b := make([]byte, 1000)
	for {
		n, _, err := l.ReadFromUDP(b)
		if err != nil {
			time.Sleep(time.Millisecond)
			continue
		}
		if !strings.HasPrefix(string(b[:n]), "\xde\xad\xbe\xef") || n != 7 {
			continue
		}

		select {
		case goPls <- b[4:n]:
		default:
		}

	}
}
